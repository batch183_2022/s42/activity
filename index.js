// // console.log("Hello World!");
// // [SECTION] Document Object  (DOM)
// // It allows us to be able to access or modify the properties of an element in a webpage.
// // It is a standard on how to get, change, add, or delete HTML elements.

// /*
// 	Syntax: 
// 		document.querySelector("htmlElement")

// 		= The quesrySelector function takes a string input that is formatted like a css selector when applying styles.
// */
// //Altenative way in retrieving elements.
// // document.getElementById("txt-first-name");
// // document.getElementByClassName("txt-last-name");

// // However, using this functions requires us to identify beforehand we get the element. With querySelector , we can be flexible in how to retrieve elements.


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const chosenColor =  document.querySelector("#colors");

// //[SECTION] Event Listeners 
// // Whenever a user interacts with a web page, this action is considered as an event. (exmaple: mouse click, mouse hover, page load, key press, etc.)

// // AddEventListener takes two arguments;
// // A string that identify an event.
// // A function that the listener will execute once the "specified event" is triggered.
// txtFirstName.addEventListener("keyup", ()=>{
// 	// ".innerHTML" property sets or returns the HTML content(inner HTML) of an element (div, spans, etc.)
// 	// ".value" property sets or returns the value of an attribute (form controls).
// 	spanFullName.innerHTML = `${txtFirstName.value}`;
// });

// // When the event occurs, an "even object" is passed to the function argument as the first parameter.
// txtFirstName.addEventListener("keyup", (event)=>{
// 	// The "event.target" contains the element where the event happened.
// 	console.log(event.target);
// 	// The "event.target.value" gets the value of the input object (txt-first-name)
// 	console.log(event.target.value);
// })

// // Creating Multiple event that use the same function.

const fullName = () =>{
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
    HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
    HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/

chosenColor.addEventListener("change", ()=> {
	spanFullName.style.color = chosenColor.value;
})

